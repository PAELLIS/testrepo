package com.abtec.TestingRepo;

import javax.baja.sys.*;

public class BSecondComponent extends BComponent {

	public static final Property timeExecuted = newProperty(0, BAbsTime.DEFAULT, null);

	public BAbsTime getTimeExecuted(){
		return (BAbsTime) get(timeExecuted);
	}

	public void setTimeExecuted(BAbsTime v){
		set(timeExecuted, v, null);
	}

	public static final Property alarmType = newProperty(0, "", null);

	public String getAlarmType(){
		return getString(alarmType);
	}

	public void setAlarmType(String v){
		setString(alarmType, v, null);
	}

	public static final Action execute = newAction(0, null);

	public void execute() { invoke(execute,null,null); }

	public void doExecute(){
		System.out.println("Alarm Type: " + getAlarmType());
		setTimeExecuted(BAbsTime.now());
		System.out.println("Time: " + BAbsTime.now());

	}

	public Type getType(){
		return TYPE;
	}
	public static final Type TYPE = Sys.loadType(BFirstComponent.class);
}

