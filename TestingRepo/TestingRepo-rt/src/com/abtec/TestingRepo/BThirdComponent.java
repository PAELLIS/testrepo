package com.abtec.TestingRepo;

import javax.baja.sys.BAbsTime;

public class BThirdComponent {
	public void doExecute() {
		// new comment
		System.out.println("Time: " + BAbsTime.now());

		// add new statement on master branch
		System.out.println("Master");

		// add new statement on Issue24
		System.out.println("Issue 24");

		// add new statement on master branch
		System.out.println("New code on Master");

		// add new statement on Issue25
		System.out.println("Issue 25");

		// add new statement on master branch
		System.out.println("Master");

		// add new statement on Issue26
		System.out.println("Issue 26");

		// add new statement on Issue27
		System.out.println("Issue 27");


		System.out.println("Issue 28");
	}
}
